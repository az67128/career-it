export async function post({ request }) {
	const data = await request.json();
	console.log(data);
	const {TELEGRAM_BOT_TOKEN, CHAT_ID } = process.env
	try {
		const res = await fetch(`https://api.telegram.org/bot${TELEGRAM_BOT_TOKEN}/sendMessage`, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({ chat_id: CHAT_ID, text: JSON.stringify(data) })
		});
		const json = await res.json();
		console.log(json);
	} catch (err) {
		console.log(err);
	}
}
